const express = require("express");
const router = express.Router();

//contollers
const userController = require("./../controllers/userControllers");
//auth
const auth = require('./../auth');


//user registration
router.post("/register", (req, res)=> {

	userController.checkEmailAndRegister(req.body).then(result => res.send(result));
})


//user authentication or login
router.post("/login", (req, res) => {

	userController.login(req.body).then( result => res.send(result));
})

//show all users (admin only)
router.get("/showAll", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)
    
    userController.showAllUsers(userData.id).then(result => res.send(result));
})


//set user as Admin (admin only)
router.put('/:userId/setAsAdmin', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	userController.setAsAdmin(userData.id, req.params.userId).then(result => res.send(result));
});

//set user as Customer (admin only)
router.put('/:userId/setAsCustomer', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	userController.setAsCustomer(userData.id, req.params.userId).then(result => res.send(result));
});

//delete user
router.delete('/:userId/deleteUser', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	userController.deleteUser(userData.id, req.params.userId).then(result => res.send(result));
});

//create order (customer only)
router.post('/createOrder', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	userController.createOrder(data).then( result => res.send(result))
})

//add product to order (customer only)
router.post('/:orderId/addProductToOrder', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId,
		price: req.body.price,
		orderId: req.params.orderId
	}

	userController.addProductToOrder(data).then( result => res.send(result))
})

//show all orders (admin only)
router.get('/showAllOrders', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}
	
	userController.showAllOrders(data).then( result => res.send(result))
})

//show user's orders (customer only)
router.get('/showUserOrders', auth.verify, (req, res) => {

	let data = {
		userId: auth.decode(req.headers.authorization).id
	}

	userController.showUserOrders(data).then(result => res.send(result))
})

//TESTING
router.get('/testing', auth.verify, (req, res) => {

	userController.testing(req.body).then( result => res.send(result))
})



module.exports = router;


