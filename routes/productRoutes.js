const express = require("express");
const router = express.Router();

//contollers
const productController = require("./../controllers/productControllers");
//auth
const auth = require('./../auth');

//create product (admin only)
router.post('/addProduct', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	productController.addProduct(userData.id, req.body).then(result => res.send(result));
});

//show all active products
router.get("/", auth.verify, (req, res) => {
    
    productController.showActiveProducts().then(result => res.send(result));
})

//show ALL products
router.get("/all", auth.verify, (req, res) => {
    
    productController.showAllProducts().then(result => res.send(result));
})

//show single product
router.get("/:productId", auth.verify, (req, res) => {
    
    productController.showProduct(req.params.productId).then(result => res.send(result));
})

//update product info (admin only)
router.put('/:productId', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	productController.updateProduct(userData.id, req.params.productId, req.body).then(result => res.send(result));
});

//archive product (admin only)
router.put('/:productId/archive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	productController.archiveProduct(userData.id, req.params.productId).then(result => res.send(result));
});

//unarchive product (admin only)
router.put('/:productId/unarchive', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	productController.unarchiveProduct(userData.id, req.params.productId).then(result => res.send(result));
});

//delete user
router.delete('/:productId/deleteProduct', auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

	productController.deleteProduct(userData.id, req.params.productId).then(result => res.send(result));
});



module.exports = router;
