const User = require("./../models/User");
const Product = require('./../models/Product');
const Order = require('./../models/Order');

//create product (admin only)
module.exports.addProduct = (data, reqBody) => {

	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return Product.find({name: reqBody.name}).then((result) => {
                if(result.length != 0){
                    return ("Sorry! " + reqBody.name + " already exists. Please enter a different product name.")
                } else {
                    return newProduct.save().then((result, error) =>{
                        if(error){
                            return error
                        } else {
                            return ("You have added a new product: " + result)
                        }
                    })
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
} 

//show all active products
module.exports.showActiveProducts = () => {

	 return Product.find({isActive:true}).then(product => {
        return product
    })
}

//show ALL products
module.exports.showAllProducts = () => {

    return Product.find().then(product => {
       return product
   })
}

//show single product
module.exports.showProduct = (params) => {

    return Product.findById(params).then(product => {
       return product
   })
}

//update product info (admin only)
module.exports.updateProduct = (data, params, reqBody) => {

    let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
    
    return User.findById(data).then(result => {
        if(result.isAdmin === true){
            return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then((result, error) => {
                if(error){
                    return false
                } else {
                    return (result.name + " is now updated." + result)
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}

//archive product (admin only)
module.exports.archiveProduct = (data, params) => {

    let updatedProduct = {
		isActive: false
	}
    
    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return Product.findById(params).then((result, error) => {
                if(error){
                    return false
                }if(result.isActive === false){
                    return (result.name + " is already archived.")
                }else{
                    return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then((result, error) => {
                        if(error){
                            return false
                        }else{
                            return (result.name + " is now archived." + result)
                        }
                    })
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}

//unarchive product (admin only)
module.exports.unarchiveProduct = (data, params) => {

    let updatedProduct = {
		isActive: true
	}
    
    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return Product.findById(params).then((result, error) => {
                if(error){
                    return false
                }if(result.isActive === true){
                    return (result.name + " is already unarchived.")
                }else{
                    return Product.findByIdAndUpdate(params, updatedProduct, {new: true}).then((result, error) => {
                        if(error){
                            return false
                        }else{
                            return (result.name + " is now unarchived." + result)
                        }
                    })
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}

//delete user (admin only)
module.exports.deleteProduct = (data, params) => {

    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return Product.findByIdAndDelete(params).then((result, error) => {
                if(error){
                    return false
                } else {
                    return ("Product deleted.")
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}