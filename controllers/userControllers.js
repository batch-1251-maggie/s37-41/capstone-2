const User = require("./../models/User");
const Product = require('./../models/Product');
const Order = require('./../models/Order');
const bcrypt = require('bcrypt');
const auth = require('./../auth');

//user registration with email check if already existing
module.exports.checkEmailAndRegister = (reqBody) => {

	let newUser = new User({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})


    return User.find({email: reqBody.email}).then( (result) => {
		if(result.length != 0){
            return ("Sorry! " + reqBody.email + " already exists. Please enter a different email to register a new account.")
		} else {
			return newUser.save().then( (result, error) =>{
                if(error){
                    return error
                } else {
                    return ("Congratulations! You have registered a new account. You may now log in using your email: " + reqBody.email)
                }
            })
		}
	})
} 

//user authentication or login
module.exports.login = (reqBody) => { 

	return User.findOne({email: reqBody.email}).then( (result) => {

		if(result == null){
			return ("Email does not exist. Please try again.")
		} else {
			const passwordCheck = bcrypt.compareSync(reqBody.password, result.password)
			if (passwordCheck === true && result.isAdmin === false){
				return ("You are logged in as Customer! Your token is: " + 
                auth.createAccessToken(result.toObject()) + " Enjoy your shopping!")
            } if (passwordCheck === true && result.isAdmin === true){
			    return ("You are logged in as Admin Your token is: " + 
                auth.createAccessToken(result.toObject()))
			} else {
				return ("Wrong password. Please try")
			}
		}
	})
}

//show all users (admin only)
module.exports.showAllUsers = (data) => {

	return User.findById(data).then( result => {
        if(result.isAdmin === true){
		    return User.find().then(result => {
                return result
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
	})
}

//set user as admin (admin only)
module.exports.setAsAdmin = (data, params) => {

    let updatedUser = {
		isAdmin: true
	}
    
    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return User.findById(params).then((result, error) => {
                if(error){
                    return false
                }if(result.isAdmin === true){
                    return (result.email + " is already an Admin." + result)
                } else {
                    return User.findByIdAndUpdate(params, updatedUser, {new: true}).then((result, error) => {
                        if(error){
                            return false
                        } else {
                            return (result.email + " is now updated from Customer to Admin." + result)
                        }
                    })
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}

//set user as customer (admin only)
module.exports.setAsCustomer = (data, params) => {

    let updatedUser = {
		isAdmin: false
	}
    
    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return User.findById(params).then((result, error) => {
                if(error){
                    return false
                }if(result.isAdmin === false){
                    return (result.email + " is already a Customer." + result)
                } else {
                    return User.findByIdAndUpdate(params, updatedUser, {new: true}).then((result, error) => {
                        if(error){
                            return false
                        } else {
                            return (result.email + " is now updated from Admin to Customer." + result)
                        }
                    })
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}

//delete user (admin only)
module.exports.deleteUser = (data, params) => {

    return User.findById(data).then( result => {
        if(result.isAdmin === true){
            return User.findByIdAndDelete(params).then((result, error) => {
                if(error){
                    return false
                } else {
                    return ("Account deleted.")
                }
            })
        } else {
            return ("You have no access to this function because you are not an Admin.")
        }
    })
}

//create order (customer only) add userId to order
module.exports.createOrder =  (data) => {

    let userId = data.userId

	let newOrder = new Order({
		userId: data.userId
	}) //add userId to new Order created

    return User.findById(userId).then( user => {
        if(user.isAdmin === false){
            return newOrder.save().then( order =>{ //userId added to new order
                user.orders.push({orderId: order.id})
                return user.save().then( (user, error) => { //orderId added to user - orders
                        if(error){
                            return false
                        } else {
                            return order
                        }
                })
            })
        } else {
            return ("You have no access to this function because you are not a Customer.")
        }
    })

}


//add product to order (customer only)
module.exports.addProductToOrder =  (data) => {

    let userId = data.userId
    let productId = data.productId
    let price = data.price
    let orderId = data.orderId
    let totalAmount = 0;

    return User.findById(userId).then( user => {
        if(user.isAdmin === false){
            return Order.findById(orderId).then(order => {
                order.items.push({productId: productId, price: price}) //adds product to order

                return order.save().then( (order) => {
                    let i = 0;
                    while (order.items[i].price !== undefined) {
                        let price = order.items[i].price;
                        totalAmount += price;
                        i++;
                        if(order.items[i] == undefined){
                            return true
                        }
                    }    
                }).then(order => {

                    let updatedTotalAmount = {"totalAmount": totalAmount} //updates total amount in order

                    return Order.findByIdAndUpdate(orderId, updatedTotalAmount, {new: true}).then(order => order);

                });
            })
        } else {
            return ("You have no access to this function because you are not a Customer.")
        }
    })

}



//show all orders (admin only)
module.exports.showAllOrders = (data) => {

    return User.findById(data.userId).then( user => {
        if(user.isAdmin === true){
            return Order.find().then(order => {
                return order
            })
        } else {
        return ("You have no access to this function because you are not an Admin.")
    }
})

}


//show user's orders (customer only)
module.exports.showUserOrders = (data) => {


    return User.findById(data.userId).then( user => {
        if(user.isAdmin === false){
            return User.findById(data.userId).then(user => {
                return user.orders
            })
        } else {
            return ("You have no access to this function because you are not a Customer.")
        }
    })

}

module.exports.testing = (reqBody) => {

    return Order.findById(reqBody.id).then( order => {
        // console.log(order.items[0].price)

        let totalAmount = 0;
        let i = 0;

        while (order.items[i].price !== undefined) {
            let price = order.items[i].price;
            totalAmount += price;
            i++;
            if(order.items[i] == undefined){
                return console.log(totalAmount)
            }
            
        }

        //  console.log(totalAmount);

    })

}

// let prices = [1000, 2000, 3000, 4000, 5000];

// let totalAmount = 0;
// let i = 0;
// // console.log(prices[5])
// while (prices[i] !== null) {
//     price = prices[i];
//     totalAmount +=  price;
//     // console.log(totalAmount);
//     i++;
// } console.log(totalAmount);