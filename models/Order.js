const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema(
		{
		userId: {
			type: String,
			required: [true, "CustomerId is required"]
		},
		totalAmount: {
			type: Number,
			default: 0
		},
		purchasedOn: {
			type: Date,
			default: new Date()
		},
		items: [
			{
				productId: {
					type: String
					// required: [true, "productId is required"]
				},
				price: {
					type: Number,
					default: 0
				}
			}
		]
	}
);

module.exports = mongoose.model("Order", orderSchema);